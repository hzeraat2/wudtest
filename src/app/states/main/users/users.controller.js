(function () {
  'use strict';

  angular
    .module('wud.techtest')
    // .config(function ($httpProvider) {
    //     delete $httpProvider.defaults.headers.common['X-Requested-With'];
    // })
    .controller('UsersController', UsersController);

  /** @ngInject */
  function UsersController($http, $log) {
    var getUrl = 'http://localhost:8000/users';
    var postUrl = 'http://localhost:8000/user';
    var vm = this;
    vm.people;

    vm.obj = {
      firstname: '',
      lastname: '',
      email: ''
    }

    getData();

    function getData() {
      $http.get(getUrl).then(function (data) {
        vm.people = data;
        $log.log(vm.people);

      })
    }
    
    vm.submitData = function(){

      //var temp = angular.toJson(d);
      if (vm.obj.firstname != '' && vm.obj.username != '' && vm.obj.email != ''){
        $http.post(postUrl, vm.obj).then(error, sucess);
      }else{
        alert('please complete user details!')
      }

      function error(e){
        $log.log(e);
      }

      function sucess(data){
        $log.log(data);
      }
    }
  }
})();
